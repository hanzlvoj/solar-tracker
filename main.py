from utime import sleep, sleep_ms
from servo import Servo
from stepper import Stepper
from ldr import read_bot_left, read_bot_right, read_up_left, read_up_right
import math

stepper = Stepper(2,3,4,5)
servo = Servo(19)

# TODO: experiment with different tolerances
tolerance = 0.07
tolerance_stepper = 0.09

stepper_dir = 1
stepper_step = 1
servo_step = 0.5

def exceeds_tolerance(val1, val2, tol):
    return abs(val1-val2) > tol


if __name__ == "__main__":
    stepper_dir_cnt = 0
    
    while True:
        dl = read_bot_left()
        dr = read_bot_right()
        tl = read_up_left()
        tr = read_up_right()
        
        avt = (tl + tr) / 2
        avd = (dl + dr) / 2 
        avl = (dl + tl) / 2 
        avr = (dr + tr) / 2
        
   
        
        if exceeds_tolerance(avt, avd, tolerance):
            if avt > avd:
                servo.angle(servo.curr_angle + servo_step)
                
            elif avt < avd:
                servo.angle(servo.curr_angle - servo_step)
        
        elif exceeds_tolerance(avl, avr, tolerance_stepper):
            if avl > avr:
                if not stepper.rotate(num_of_steps=stepper_step, direction=stepper_dir):
                    stepper_dir_cnt += 1
            
            elif avl < avr:
                if not stepper.rotate(num_of_steps=stepper_step, direction=-stepper_dir):
                   stepper_dir_cnt += 1 
            
            if stepper_dir_cnt > 50:
                # If the stepper is at its bounds for long enough, switch its direction of rotation
                stepper_dir_cnt = 0
                stepper_dir = -1 * stepper_dir
                   
        
            
            
    
    
    
    
    
    