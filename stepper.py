from utime import sleep, sleep_ms
from machine import Pin

def clamp(value, min_value, max_value):
    if value < min_value:
        return min_value
    if value > max_value:
        return max_value
    return value

class Stepper:
    """
        512 steps = 360 degree rotation
        min/max_pos is used to keep the motor's rotation within these bounds
    """
    def __init__(self, in1, in2, in3, in4, min_pos = 0, max_pos = 256):
        self.IN1 = Pin(in1, Pin.OUT)
        self.IN2 = Pin(in2, Pin.OUT)
        self.IN3 = Pin(in3, Pin.OUT)
        self.IN4 = Pin(in4, Pin.OUT)
        
        self.min_pos = min_pos
        self.max_pos = max_pos
        self.curr_pos = None
        
        # half step sequence for clock wise rotation
        self.seq = [
            [1, 0, 0, 1],
            [0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 1, 0, 0],
            [1, 1, 0, 0],
            [1, 0, 0, 0],
            ]
        
        self.calibrate()

    
    def rotate(self, num_of_steps, direction=1, delay=0.002):
        """
        direction = 1 for clockwise rotation, -1 for counter clock wise rotation
            
        negative num_of_steps counts as counter clock wise rotation regardless of direction param
            
        calculates new position, checks if calculated position is within bounds, rotates the motor accordingly
        """
        
        if num_of_steps < 0:
            direction = -1
            num_of_steps = -num_of_steps
        
        new_pos = self.curr_pos + (num_of_steps * direction)
        
        if new_pos >= self.min_pos and new_pos <= self.max_pos:
            self._take_steps(num_of_steps, direction, delay)
            self.curr_pos = clamp(new_pos, self.min_pos, self.max_pos)
            return True
        return False
                    
      
    
    def _take_steps(self, num_of_steps, direction=1, delay=0.002):
        """
            private function, not supposed to be used outside of this module
            
            direction = 1 for clockwise rotation, -1 for counter clock wise rotation
            
            initializes motor pins, rotates by num_of_steps and deinitializes pins
        """
        self.init_pins_OUT()
        for _ in range(num_of_steps):
            for step in self.seq[::direction]:
                self.IN1.value(step[0])
                self.IN2.value(step[1])
                self.IN3.value(step[2])
                self.IN4.value(step[3])
                sleep(delay)
        self.init_pins_IN()


    def init_pins_IN(self):
        self.IN1.init(Pin.IN)
        self.IN2.init(Pin.IN)
        self.IN3.init(Pin.IN)
        self.IN4.init(Pin.IN)
        
    def init_pins_OUT(self):
        self.IN1.init(Pin.OUT)
        self.IN2.init(Pin.OUT)
        self.IN3.init(Pin.OUT)
        self.IN4.init(Pin.OUT)
        
        
    def calibrate(self):
        """
            rotates the motor by max num of steps, so that no matter what its actual position is, it gets always stopped by a physical block
        """
        self._take_steps(num_of_steps=self.max_pos, direction=-1)
        sleep(0.5)
        self._take_steps(num_of_steps=1, direction=1)
        sleep(0.5)
        self.curr_pos = 1
        
        






 
