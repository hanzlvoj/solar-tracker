from machine import Pin, ADC, PWM
from utime import sleep, sleep_ms


mux_select_S0 = Pin(16, Pin.OUT)
mux_select_S1 = Pin(17, Pin.OUT)
mux_select_S2 = Pin(18, Pin.OUT)
mux_enable = Pin(1, Pin.OUT)
mux_enable.value(0)

adc = ADC(26)

adc2 = ADC(28)

convert_factor = 3.3/65535

def read_28():
    return adc2.read_u16()*convert_factor


def select_mux_A7():
    "bottom left"
    mux_select_S0.value(1)
    mux_select_S1.value(1)
    mux_select_S2.value(1)

def select_mux_A5():
    "bottom right"
    mux_select_S0.value(1)
    mux_select_S1.value(0)
    mux_select_S2.value(1)

def select_mux_A0():
    "upper left"
    mux_select_S0.value(0)
    mux_select_S1.value(0)
    mux_select_S2.value(0)
   
def select_mux_A3():
    "upper right"
    mux_select_S0.value(1)
    mux_select_S1.value(1)
    mux_select_S2.value(0)
    
def read_bot_left():
    select_mux_A7()
    return adc.read_u16()*convert_factor

def read_bot_right():
    select_mux_A5()
    return adc.read_u16()*convert_factor

def read_up_left():
    select_mux_A0()
    return adc.read_u16()*convert_factor

def read_up_right():
    select_mux_A3()
    return adc.read_u16()*convert_factor


'''
if __name__ == "__main__":  
    select_mux_A7()
    print("bottom left:")
    print(adc.read_u16()*convert_factor)
    sleep_ms(1000)
       
    select_mux_A5()
    print("bottom right:")
    print(adc.read_u16()*convert_factor)
    sleep_ms(1000)
       
    select_mux_A0()
    print("upper left:")
    print(adc.read_u16()*convert_factor)
    sleep_ms(1000)
     
    select_mux_A3()
    print("upper right:")
    print(adc.read_u16()*convert_factor)
    sleep_ms(1000)
'''