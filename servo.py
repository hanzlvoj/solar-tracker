from machine import Pin, PWM, ADC, disable_irq, enable_irq
from utime import sleep, sleep_ms


"""
max_ns = 2_450_000
mid_ns = 1_410_000
min_ns = 525_000
"""
    
    
    
def clamp(value, min_value, max_value):
    if value < min_value:
        return min_value
    if value > max_value:
        return max_value
    return value    
    
    
class Servo:

    
    def __init__(self, pin: int, max_ns = 2_450_000, mid_ns = 1_410_000, min_ns = 525_000, min_angle = 40, max_angle = 105):
        self.pin = Pin(pin)
        self.pwm = PWM(self.pin)
        self.pwm.freq(50)
        
        self.max_ns = max_ns
        self.mid_ns = mid_ns
        self.min_ns = min_ns
        
        self.min_angle = min_angle
        self.max_angle = max_angle
        self.curr_angle = None
        self.calibrate()
        
        
    def calibrate(self):
        init_angle = (self.min_angle+self.max_angle)//2
        self.angle(init_angle)
        sleep(0.5)
        

    def clamped_angle_to_duty(self, angle):
        clamped_angle = clamp(angle, self.min_angle, self.max_angle)
        return int(clamped_angle * (self.max_ns - self.min_ns) / 180 + self.min_ns)

    def angle(self, angle, delay=0.015):
        if angle >= self.min_angle and angle <= self.max_angle:
            duty = self.clamped_angle_to_duty(angle)
            self.pwm.duty_ns(duty)
            self.curr_angle = angle
            sleep(delay)
        
    def deinit(self):
        self.pwm.deinit()
        
    
  
s = Servo(19)
"""
while True:

    while s.curr_angle > s.min_angle:
        s.angle(s.curr_angle-1)
        

    while s.curr_angle < s.max_angle:
        s.angle(s.curr_angle+1)
    sleep(0.5)
    s.deinit()
"""



        
